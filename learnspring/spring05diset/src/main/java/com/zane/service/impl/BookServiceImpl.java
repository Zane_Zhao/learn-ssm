package com.zane.service.impl;

import com.zane.dao.BookDao;
import com.zane.dao.UserDao;
import com.zane.service.BookService;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BookServiceImpl implements BookService {
    // 5. 删除业务层中使用new方式创建的Dao对象
//    private BookDao bookDao = new BookDaoImpl();
    private BookDao bookDao;
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public void save() {
        System.out.println("book service save ...");
        bookDao.save();
        userDao.save();
    }
}
