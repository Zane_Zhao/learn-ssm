package com.zane;

import com.zane.dao.OrderDao;
import com.zane.dao.UserDao;
import com.zane.factory.OrderDaoFactory;
import com.zane.factory.UserDaoFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppForInstanceUser {
    public static void main(String[] args) {
        // 一般模式
//        // 不是静态方法，需要使用对象调用
//        // 创建实例工厂对象
//        UserDaoFactory userDaoFactory = new UserDaoFactory();
//        // 通过实例工厂创建对象
//        UserDao userDao = userDaoFactory.getUserDao();
//        userDao.save();

        // 配置Spring容器模式
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        UserDao userDao = (UserDao) ctx.getBean("userDao");

        userDao.save();

    }
}
