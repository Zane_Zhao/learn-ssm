package com.zane.factory;

import com.zane.dao.Impl.OrderDaoImpl;
import com.zane.dao.Impl.UserDaoImpl;
import com.zane.dao.OrderDao;
import com.zane.dao.UserDao;

public class UserDaoFactory {
    public UserDao getUserDao() {
        return new UserDaoImpl();
    }
}
