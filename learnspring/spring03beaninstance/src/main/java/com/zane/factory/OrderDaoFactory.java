package com.zane.factory;

import com.zane.dao.Impl.OrderDaoImpl;
import com.zane.dao.OrderDao;

public class OrderDaoFactory {
    public static OrderDao getOrderDao() {
        System.out.println("factory setup...");
        return new OrderDaoImpl();
    }
}
