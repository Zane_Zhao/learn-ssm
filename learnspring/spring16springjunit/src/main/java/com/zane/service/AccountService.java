package com.zane.service;

import com.zane.domain.Account;

import java.util.List;

public interface AccountService {

    void save(Account account);

    void update(Account account);

    void delete(Integer id);

    Account findById(Integer id);

    public List<Account> findAll();
}
