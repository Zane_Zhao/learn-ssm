package com.zane.service;

import com.zane.config.SpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// 测试业务层接口
// 使用spring整合junit的方式运行程序
@RunWith(SpringJUnit4ClassRunner.class) // 使用spring整合junit的专业类运行器
@ContextConfiguration(classes = SpringConfig.class)  // 指定Spring上下文的配置类在哪
// Spring整个JUnit就以上两行注解，且是固定格式

public class AccountServiceTest {
    // 如果想之成为测试用例，必须要导入JUnit包

    // 测业务层接口
    @Autowired // 自动装配，保证能够使用（需要测试那个bean，就直接注入）
    private AccountService accountService;

    // 测试方法
    @Test
    public void testFindById() {
        System.out.println(accountService.findById(1));
    }
    @Test
    public void testFindAll(){
        System.out.println(accountService.findAll());
    }

}
