package com.zane.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.sql.DataSource;

@Configuration
//@ComponentScan("com.zane.config")
@ComponentScan("com.zane")  // 注解需要使用@ComponentScan识别扫描对应的包
@Import({JdbcConfig.class})
public class SpringConfig {
}
