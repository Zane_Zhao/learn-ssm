package com.zane.service.impl;

import com.zane.dao.BookDao;
import com.zane.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//@Component
// 衍生注解:和Component完全一样，但是指明是业务层的bean
@Service
public class BookServiceImpl implements BookService {
    // 5. 删除业务层中使用new方式创建的Dao对象 （注入Dao）
//    private BookDao bookDao = new BookDaoImpl();
    @Autowired  // 按照类型装配 并且使用暴力反射直接加值 不需要setter方法注入
    @Qualifier("bookDao") // 使用名称识别，使用率低
    private BookDao bookDao;

    public void save() {
        System.out.println("book service save ...");
        bookDao.save();
    }


}
