package com.zane;

import com.zane.dao.BookDao;
import com.zane.service.BookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class App2 {
    public static void main(String[] args) {

        // 1. 加载类路径下的配置文件
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        // 2. 从文件系统(绝对路径)下加载配置文件
        ApplicationContext ctx2 = new FileSystemXmlApplicationContext("D:\\laboratory\\code\\learn-ssm\\learnspring\\spring10container\\src\\main\\resources\\applicationContext.xml");
        // Ctrl + H 打开ApplicationContext接口的（容器）类继承层次结构图
        // 子接口对父接口的功能追加
//      BookDao bookDao = (BookDao) ctx2.getBean("bookDao");// Ctrl + Shift + Enter 补全分号  1. 使用bean名称获取
//      BookDao bookDao = ctx2.getBean("bookDao", BookDao.class);  2. 使用bean名称获取并指定类型
        BookDao bookDao = ctx2.getBean(BookDao.class);  // 3. 使用bean类型获取
        bookDao.save();


    }
}
