package com.zane.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component // 让之成为被Spring控制加载的Spring bean，告诉Spring来加载这个类
@Aspect // 告诉Spring当做AOP处理，代码生效
// 定义通知类，制作通知  随便写类与方法无参无返回值，写个功能即可
public class MyAdvice {
    // 定义切入点
//    @Pointcut("execution(void com.zane.dao.BookDao.update())")
//    @Pointcut("execution(void com.zane.dao.Impl.BookDaoImpl.update())")
//    @Pointcut("execution(* com.zane.dao.Impl.BookDaoImpl.update(*))")  No
//    @Pointcut("execution(* com.*.*.*.*.update())")  Yse
//    @Pointcut("execution(* com.*.*.*.update())") Yes  三层匹配接口
//    @Pointcut("execution(* *..update())")  Yes *..表示任意
//    @Pointcut("execution(* *..*(..))")  匹配所有
//    @Pointcut("execution(* *..u*(..))")
//    @Pointcut("execution(* com.zane.*.*Service.find*(..))")  // 给所有的业务层查询方法加载（挂上）aop
    @Pointcut("execution(* com.zane.*.*Service.save*(..))")  // 给所有的业务层保存方法加载（挂上）aop

    // 设定私有的无返回值无参无功能的方法（方法名()就是那个连接）
    private void pt(){}

    @Before("pt()") //绑定切入点和通知
    // 定义共性功能（通知Advice）
    public void method(){
        System.out.println(System.currentTimeMillis());
    }
}
