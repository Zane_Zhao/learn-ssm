package com.zane;

import com.zane.service.BookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppSystemProperties {
    public static void main(String[] args) {
        // 获取Ioc容器
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        // 获取bean

//        BookDao bookDao = (BookDao) ctx.getBean("bookDao");// Ctrl + Shift + Enter 补全分号
//
//        bookDao.save();

        BookService bookService = (BookService) ctx.getBean("bookService");
        bookService.save();

    }
}
