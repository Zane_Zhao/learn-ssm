package com.zane.dao.Impl;

import com.zane.dao.BookDao;

public class BookDaoImpl implements BookDao{
    private String name;
    private String zanename;

    public void setName(String name) {
        this.name = name;
    }

    public void setZanename(String zanename) {
        this.zanename = zanename;
    }

    public void save() {
        System.out.println("book dao save ..." + name + zanename);
    }


}
