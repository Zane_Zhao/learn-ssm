package com.zane;

import com.zane.config.SpringConfig;
import com.zane.dao.BookDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
        BookDao bookDao = ctx.getBean(BookDao.class);
//        System.out.println(bookDao.update());
        int num = bookDao.select();  // 接回返回值
        System.out.println(num);
/*        System.out.println(bookDao);
        System.out.println(bookDao.getClass());*/
    }
}
