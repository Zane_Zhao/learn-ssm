package com.zane.aop;
// 通知类
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MyAdvice {
    @Pointcut("execution(void com.zane.dao.BookDao.update())")
    private void pt(){}
    @Pointcut("execution(int com.zane.dao.BookDao.select())")
    private void pt2(){}

//    @Before("pt()")
    public void before() {
        System.out.println("before advice");
    }

//    @After("pt()")
    public void after() {
        System.out.println("after advice");
    }

//    @Around("pt()")
//    环绕通知依赖形参 ProceedingJoinPoint  其为对原始方法的调用（不调用则有隔离效果）可以用来做权限校验
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("around before advice");
        // 表示对原始操作的调用
        Object ret = pjp.proceed();
        System.out.println("around after advice");
        return ret; // null  返回对象后
    }

//    @Around("pt2()") // 环绕通知对原始方法调用后其返回值可以接出来
    public Object aroundSelect(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("around before advice");
        // 表示对原始操作的调用
        Integer ret = (Integer) pjp.proceed();
        System.out.println("around after advice");
        return ret+566;
    }

//    只有当方法没有抛出异常从而正常结束时才能运行（使用较少）
//    @AfterReturning("pt2()")
    public void afterReturning() {
        System.out.println("afterReturning advice");
    }

//    抛出异常后才运行的（使用较少）
    @AfterThrowing("pt2()")
    public void afterThrowing() {
        System.out.println("afterThrowing advice");
    }
}
