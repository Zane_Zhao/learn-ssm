import com.zane.config.SpringConfig;
import com.zane.dao.BookDao;
import com.zane.service.BookService;
import com.zane.service.impl.BookServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppForAnnotation {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
        BookDao bookDao = (BookDao) ctx.getBean("bookDao");// Ctrl + Shift + Enter 补全分号
        bookDao.save();
        System.out.println(bookDao);
        BookService bookService = ctx.getBean(BookServiceImpl.class);
        System.out.println(bookService);
    }
}
