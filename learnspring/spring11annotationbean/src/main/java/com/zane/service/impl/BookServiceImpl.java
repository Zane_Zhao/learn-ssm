package com.zane.service.impl;

import com.zane.dao.BookDao;
import com.zane.service.BookService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//@Component
// 衍生注解:和Component完全一样，但是指明是业务层的bean
@Service
public class BookServiceImpl implements BookService {
    // 5. 删除业务层中使用new方式创建的Dao对象
//    private BookDao bookDao = new BookDaoImpl();
    private BookDao bookDao;

    // 6. 提供对应的set方法
    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public void save() {
        System.out.println("book service save ...");
        bookDao.save();
    }


}
