package com.zane;

import com.zane.dao.BookDao;
import com.zane.service.BookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppForLifeCycle {
    public static void main(String[] args) {
        // 获取Ioc容器
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        // 获取bean
        BookDao bookDao = (BookDao) ctx.getBean("bookDao");
        bookDao.save();
        ctx.close();  // 暴力关闭容器
//        ctx.registerShutdownHook(); // 注册关闭钩子，不影响程序运行

    }
}
