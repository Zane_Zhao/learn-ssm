package com.zane.aop;
// 通知类
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Aspect
public class MyAdvice {
    @Pointcut("execution(* com.zane.dao.BookDao.findName(..))") // 注意参数位置不能为空，需要是.. 同时切入点的返回值类型是*,因为查询函数返回String
//  否则不会报错，但是得不到正确的结果
    private void pt(){}

    @Before("pt()")
    public void before(JoinPoint jp) {
        Object[] args = jp.getArgs();
        System.out.println(Arrays.toString(args));
        System.out.println("before advice");
    }

//    @After("pt()")
    public void after() {
        System.out.println("after advice");
    }

//    @Around("pt()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        Object ret = pjp.proceed();
        return ret;
    }

//    @AfterReturning("pt()")
    public void afterReturning() {
        System.out.println("afterReturning advice");
    }

//    @AfterThrowing("pt()")
    public void afterThrowing() {
        System.out.println("afterThrowing advice");
    }
}
