package com.zane.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("com.zane")
@EnableAspectJAutoProxy // 告诉Spring我们要用注解开发AOP(启动MyAdvice中的@Aspect)
public class SpringConfig {
}
