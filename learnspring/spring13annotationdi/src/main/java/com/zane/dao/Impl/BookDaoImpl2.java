package com.zane.dao.Impl;

import com.zane.dao.BookDao;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

//@Component("bookDao")
@Repository("bookDao2")
//@Scope("prototype")
public class BookDaoImpl2 implements BookDao {
    public void save() {
        System.out.println("book dao save 2...");
    }

}
