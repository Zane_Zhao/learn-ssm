package com.zane.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.zane")
@PropertySource({"jdbc.properties"})  // 此处不支持通配符*
public class SpringConfig {
}
