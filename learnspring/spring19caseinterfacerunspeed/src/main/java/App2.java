import com.zane.config.SpringConfig;
import com.zane.domain.Account;
import com.zane.service.AccountService;
import com.zane.service.impl.AccountServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App2 {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

        // 不需要拿dao，因为dao已经注入到service
        // .class 按类型拿，因为未设置名称
        AccountService accountService = ctx.getBean(AccountService.class);

        Account ac = accountService.findById(1);
//        System.out.println(ac);
    }
}
