package com.zane.config;
// 快速开发，简化开发，注解
// 挂上spring的纯注解开发格式

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.zane")  // 包的扫描结构可以设置大一点
@PropertySource("classpath:jdbc.properties")  // 加载配置文件
@Import({JdbcConfig.class, MybatisConfig.class})  // 加载JDBC和Mybatis的配置
@EnableAspectJAutoProxy // 打开AOP注解
public class SpringConfig {
}
