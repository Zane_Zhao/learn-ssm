package com.zane.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

// 做方法返回SqlSessionFactory对象即可
public class MybatisConfig {

    // FactoryBean都是造对象的
    // 使用@Bean注解定义的bean要想注入引用类型在形参上加对应参数即可
    @Bean
    public SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource){
        SqlSessionFactoryBean ssfb = new SqlSessionFactoryBean();
        ssfb.setTypeAliasesPackage("com.zane.domain");  // 扫描类型别名的包
        ssfb.setDataSource(dataSource);
        return ssfb;
    }

    // 映射扫描
    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer(){
        MapperScannerConfigurer msc = new MapperScannerConfigurer();
        msc.setBasePackage("com.zane.dao"); // 扫描映射的包
        return msc;
    }
}
