// 对应的数据层的操作接口
package com.zane.dao;

import com.zane.domain.Account;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import java.util.List;
// 数据层，采用MyBatis注解开发  接口映射
// 注解形式配置的映射关系（配置文件中就不会有映射的.xml文件）
// 没有实现类，自动代理出实现类对象
public interface AccountDao {

    @Insert("insert into tb1_account(name, money)values(#{name}, #{money})")
    void save(Account account);

    @Delete("delete from tb1_account where id = #{id}")
    void  delete(Integer id);

    @Update("update tb1_account set name = #{name}, money = #{money} where id = #{id}")
    void update(Account account);

    @Select("select * from tb1_account")
    List<Account> findAll();

    @Select("select * from tb1_account where id = #{id}")
    Account findById(Integer id);
}
