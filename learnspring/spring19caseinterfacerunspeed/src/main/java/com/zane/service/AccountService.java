package com.zane.service;
import com.zane.domain.Account;
import java.util.List;
// Spring整合JDBC的一套代码
// 业务层接口，增删改查（查全部，单个）
public interface AccountService {
    void save(Account account);
    void update(Account account);
    void delete(Integer id);
    public List<Account> findAll();
    Account findById(Integer id);
}
