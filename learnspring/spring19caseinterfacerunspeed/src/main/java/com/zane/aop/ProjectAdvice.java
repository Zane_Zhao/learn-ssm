package com.zane.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ProjectAdvice {
    // 匹配业务层的所有方法
    @Pointcut("execution(* com.zane.service.*Service.*(..))")
    private void servicePt(){}  // 空方法体，但是有壳

    @Around("ProjectAdvice.servicePt()")
    public void runSpeed(ProceedingJoinPoint pjp) throws Throwable {
        // 获取执行签名的对象
        Signature signature = pjp.getSignature();
/*        System.out.println(signature.getDeclaringType()); // 对应类型
        System.out.println(signature.getName()); // 对应方法名
        System.out.println(signature.getDeclaringTypeName()); // 对应接口类型简洁*/

        String className = signature.getDeclaringTypeName();
        String methodName = signature.getName();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            pjp.proceed();
        }
        long end = System.currentTimeMillis();
        System.out.println("万次执行 " + className + "." + methodName + "---->"+ (end - start) + "ms");
    }
}
