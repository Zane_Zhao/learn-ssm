package com.zane.service;
// Junit整合的测试类
import com.zane.config.SpringConfig;
import com.zane.domain.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

// 测试业务层接口
// 使用spring整合junit的方式运行程序
@RunWith(SpringJUnit4ClassRunner.class) // 使用spring整合junit的专业类运行器
@ContextConfiguration(classes = SpringConfig.class)  // 指定Spring上下文的配置类在哪
// Spring整个JUnit就以上两行注解，且是固定格式

public class AccountServiceTest {
    // 如果想之成为测试用例，必须要导入JUnit包
    // 测业务层接口
    @Autowired // 自动装配，保证能够使用（需要测试那个bean，就直接注入）
    private AccountService accountService;
    // 测试方法
    @Test
    public void testFindById() {
        Account ac = accountService.findById(2);
//        System.out.println(ac);
    }
    @Test
    public void testFindAll(){
        List<Account> all = accountService.findAll();
//        System.out.println(all);
    }
}
