package com.zane.dao.Impl;

import com.zane.dao.BookDao;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

//@Component("bookDao")
@Repository("bookDao")
//@Scope("prototype")
public class BookDaoImpl implements BookDao {
    public void save() {
        System.out.println("book dao save ...");
    }

    @PostConstruct // 构造方法后
    public void init() {
        System.out.println("init ...");
    }

    @PreDestroy  // 销毁前
    public void destroy() {
        System.out.println("destroy ...");
    }
}
