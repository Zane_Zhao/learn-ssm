import com.zane.dao.AccountDao;
import com.zane.domain.Account;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

// MyBatis的独立运行程序
// Spring是用来管理bean的
public class App {
    public static void main(String[] args) throws IOException {
        // 初始化SqlSessionFactory核心对象
        // 1. 创建 SqlSessionFactoryBuilder对象
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        // 2. 加载SqlMapConfig.xml配置文件
        InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml.bak");
        // 3. 创建SqlSessionFactory对象（最核心对象）
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);

        // 获取连接SQLSession对象，获取实现（数据层操作接口Mapper对象）
        // 4. 获取具体的SqlSession操作连接
        SqlSession sqlSession = sqlSessionFactory.openSession(); // 类似于连接池
        // 5. 执行SqlSession对象执行查询，获取结果User（通过getMapper获取到真正要执行业务的数据层接口）
        AccountDao accountDao = sqlSession.getMapper(AccountDao.class); // 直接执行业务，但不是根源对象核心对象

        // 获取数据层接口（打包ac）
        Account ac = accountDao.findById(2);
        System.out.println(ac);

        // 关闭连接
        // 6. 释放资源
        sqlSession.close();

    }
}
